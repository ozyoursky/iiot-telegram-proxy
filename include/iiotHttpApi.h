#ifndef TGBOT_IIOTHTTPCLIENT_H
#define TGBOT_IIOTHTTPCLIENT_H

#include <string>
#include <utility>
#include "vector"

struct httpHeader {
    std::string key;
    std::string value;
};

extern std::string host;
extern std::string port;
extern std::string rootPath;
const std::string GET_METHOD = "GET";
const std::string POST_METHOD = "POST";
const std::string PUT_METHOD = "PUT";
const std::string DELETE_METHOD = "DELETE";

std::string sendGet(const std::string &path);

std::string sendPost(const std::string &path, const std::string& requestBody);

std::string sendPut(const std::string &path, const std::string& responseBody);

std::string sendDelete(const std::string &path);

#endif