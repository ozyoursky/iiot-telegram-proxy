#ifndef TGBOT_IIOTPROXYBOT_H
#define TGBOT_IIOTPROXYBOT_H

#include <vector>
#include "tgbot/tgbot.h"
#include <string>

struct staticCommand{
    std::string command;
    std::string response;
};

TgBot::BotCommand::Ptr newCommand(std::string cmd, std::string desc);

std::vector<TgBot::BotCommand::Ptr> buildCommands();

extern std::vector<staticCommand> staticCommands;

#endif
