#ifndef TGBOT_EN_H
#define TGBOT_EN_H

#include <string>

const std::string INVALID_COMMAND_RESPONSE = "Provided command not supported, try to use /build";

const std::string START_COMMAND = "start";
const std::string START_DESCRIPTION = "shows preview";
const std::string HELLO_MESSAGE = "This is a bot for sending requests to the iiot API.\n"
                                  "You can either send a custom command like GET /api/V1/Box/5,\n"
                                  "or use the builder by /build command.\n"
                                  "To view an example of the request body type /examples";

const std::string BUILD_COMMAND = "build";
const std::string BUILD_DESCRIPTION = "helps you build command";
const std::string BUILD_RESPONSE = "You can send requests to:\n"
                                   "/database_boxes\n"
                                   "/database_periphery\n"
                                   "/boxes\n"
                                   "/periphery\n"
                                   "/samples\n";

const std::string DATABASE_BOXES_COMMAND = "database_boxes";
const std::string DATABASE_BOXES_RESPONSE = "this section support:\n"
                                            "/GET Db/Boxes\n"
                                            "/GET Db/Boxes/{index}\n"
                                            "/PUT Db/Boxes/{index}\n"
                                            "/POST Db/Boxes\n"
                                            "/DELETE Db/Boxes/{index}\n";

const std::string DATABASE_PERIPHERY_COMMAND = "database_periphery";
const std::string DATABASE_PERIPHERY_RESPONSE = "this section support:\n"
                                                "/GET Db/Peripheries\n"
                                                "/GET Db/Peripheries/{index}\n"
                                                "/PUT Db/Peripheries/{index}\n"
                                                "/POST Db/Peripheries\n"
                                                "/DELETE Db/Peripheries/{index}";

const std::string BOXES_COMMAND = "boxes";
const std::string BOXES_RESPONSE = "this section support:\n"
                                   "/GET Box\n"
                                   "/GET Box/{index}\n"
                                   "/POST Box\n"
                                   "/PUT Box/{index}\n";

const std::string PERIPHERY_COMMAND = "periphery";
const std::string PERIPHERY_RESPONSE = "this section support:\n"
                                       "/GET Periphery\n"
                                       "/GET Periphery/{index}\n"
                                       "/POST Periphery\n"
                                       "/PUT Periphery/{index}\n";

const std::string SAMPLES_COMMAND = "samples";
const std::string SAMPLES_RESPONSE = "this section support:\n"
                                     "/GET Samples\n"
                                     "/GET Samples/{index}\n"
                                     "/POST Samples\n"
                                     "/PUT Samples/{index}\n"
                                     "/DELETE ApiWithActions/{index}";

const std::string GET_COMMAND = "GET";
const std::string POST_COMMAND = "POST";
const std::string PUT_COMMAND = "PUT";
const std::string DELETE_COMMAND = "DELETE";


const std::string EXAMPLES_COMMAND = "examples";
const std::string EXAMPLES_DESCRIPTION = "shows some json body examples";
const std::string EXAMPLES_RESPONSE = "some json-examples:\n"
                                      "/example_box_json\n"
                                      "/example_periphery_json\n";

const std::string EXAMPLE_BOX_JSON_COMMAND = "example_box_json";
const std::string EXAMPLE_BOX_JSON = "{"
                                     "    Id: 1"
                                     "    Temperature: 10.0,"
                                     "    Humidity: 80.0,"
                                     "    Pressure: 1.0,"
                                     "    Relay: true,"
                                     "    VoltageSupply: true"
                                     "}";

const std::string EXAMPLE_PERIPHERY_JSON_COMMAND = "example_periphery_json";
const std::string EXAMPLE_PERIPHERY_JSON = "{"
                                           "    Id: 1,"
                                           "    FirstRelay: true,"
                                           "    SecondRelay: true,"
                                           "    VoltageSupply: true"
                                           "}";


const std::string T = "";

#endif //TGBOT_EN_H
