#include "iiotBotCore.h"
#include "strings/en.h"

std::vector<staticCommand> staticCommands{
        {START_COMMAND, HELLO_MESSAGE},
        {BUILD_COMMAND, BUILD_RESPONSE},
        {DATABASE_BOXES_COMMAND, DATABASE_BOXES_RESPONSE},
        {DATABASE_PERIPHERY_COMMAND, DATABASE_PERIPHERY_RESPONSE},
        {BOXES_COMMAND, BOXES_RESPONSE},
        {PERIPHERY_COMMAND, PERIPHERY_RESPONSE},
        {SAMPLES_COMMAND, SAMPLES_RESPONSE},
        {EXAMPLES_COMMAND, EXAMPLES_RESPONSE},
        {EXAMPLE_BOX_JSON_COMMAND, EXAMPLE_BOX_JSON},
        {EXAMPLE_PERIPHERY_JSON_COMMAND, EXAMPLE_PERIPHERY_JSON}
};

TgBot::BotCommand::Ptr newCommand(std::string cmd, std::string desc) {
    TgBot::BotCommand::Ptr command(new TgBot::BotCommand);
    command->command = move(cmd);
    command->description = move(desc);
    return command;
}

std::vector<TgBot::BotCommand::Ptr> buildCommands() {
    std::vector<TgBot::BotCommand::Ptr> commands{
            newCommand(START_COMMAND, START_DESCRIPTION),
            newCommand(BUILD_COMMAND, BUILD_DESCRIPTION),
            newCommand(EXAMPLES_COMMAND, EXAMPLES_DESCRIPTION)
    };
    return commands;
}