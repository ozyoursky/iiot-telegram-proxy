#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <string>

#include <tgbot/tgbot.h>
#include "iiotBotCore.h"
#include "strings/en.h"
#include "iiotHttpApi.h"
#include "config.h"

using namespace std;
using namespace TgBot;
using namespace boost::asio;

bool isDebug = false;

void log(const string &message) {
    if (isDebug)
        cout << "DEBUG: " + message + "\n";
}

string cutFromFirstOf(const string &text, const string &str) {
    return text.substr(text.find_first_of(str) + 1);
}

string parsePlaneMessage(const Message::Ptr &message) {
    return cutFromFirstOf(message->text, " ");
}

void registerBotListeners(Bot &bot) {
    for (auto &staticCommand : staticCommands) {
        bot.getEvents().onCommand(
                staticCommand.command,
                [&bot, &staticCommand](const Message::Ptr &message) {
                    bot.getApi().sendMessage(message->chat->id, staticCommand.response);
                });
    }

    bot.getEvents().onCommand(
            GET_COMMAND,
            [&bot](const Message::Ptr &message) {
                log("received message:\n" + message->text);
                auto path = cutFromFirstOf(message->text, GET_COMMAND);
                auto response = sendGet(path);
                log("send response:\n" + response);
                bot.getApi().sendMessage(message->chat->id, response);
            });

    bot.getEvents().onCommand(
            PUT_COMMAND,
            [&bot](const Message::Ptr &message) {
                auto messText = parsePlaneMessage(message);
                auto response = sendPut(
                        messText.substr(0, messText.find_first_of('\n')),
                        cutFromFirstOf(messText, "\n")
                );
                bot.getApi().sendMessage(message->chat->id, response);
            });

    bot.getEvents().onCommand(
            POST_COMMAND,
            [&bot](const Message::Ptr &message) {
                auto messText = parsePlaneMessage(message);
                auto response = sendPost(
                        messText.substr(0, messText.find_first_of('\n')),
                        cutFromFirstOf(messText, "\n")
                );
                bot.getApi().sendMessage(message->chat->id, response);
            });

    bot.getEvents().onCommand(
            DELETE_COMMAND,
            [&bot](const Message::Ptr &message) {
                auto path = cutFromFirstOf(message->text, GET_COMMAND);
                auto response = sendDelete(path);
                bot.getApi().sendMessage(message->chat->id, response);
            });

    bot.getEvents().onUnknownCommand(
            [&bot](const Message::Ptr &message) {
                bot.getApi().sendMessage(message->chat->id, INVALID_COMMAND_RESPONSE);
            });
}

int main(int argc, char *argv[]) {
    if (argc > 1 && !strcmp(argv[1], "-d")) {
        isDebug = true;
    }

    if (argc == 5) {
        host = argv[2];
        port = argv[3];
        rootPath = argv[4];
    }
    Bot bot(botToken);
    registerBotListeners(bot);
    bot.getApi().setMyCommands(buildCommands());

    signal(SIGINT, [](int s) {
        printf("SIGINT got\n");
        exit(0);
    });

    printf("Bot username: %s\n", bot.getApi().getMe()->username.c_str());
    bot.getApi().deleteWebhook();

    TgLongPoll longPoll(bot);
    while (true) {
        try {
            printf("Long poll started\n");
            longPoll.start();
        } catch (exception &e) {
            printf("error: %s\n", e.what());
        }
    }
}

