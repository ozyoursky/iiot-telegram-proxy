#include "../include/iiotHttpApi.h"
#include <string>

#include <boost/asio.hpp>
#include <cstddef>

using namespace std;
using namespace boost::asio;
using namespace boost::asio::ip;

string host = "127.0.0.1";
string port = "8080";
string rootPath = "testIIot";

tcp::socket connectSocket() {
    boost::asio::io_service io_service;

    tcp::resolver resolver(io_service);
    tcp::resolver::query query(host, port);
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

    tcp::socket socket(io_service);
    boost::asio::connect(socket, endpoint_iterator);
    return socket;
}

string buildRequest(const string &method, const string &path, const vector<httpHeader> &headers = {}) {
    string request =
            method + " /" + path + " HTTP/1.0\r\n" +
            "Host: " + host + "\r\n" +
            "Accept: */*\r\n";
    for (const auto &header: headers) {
        request += header.key + ": " + header.value + "\r\n";
    }
    request += "Connection: close\r\n\r\n";
    return request;
}

string buildRequest(const string &method, const string &path, const string &body, vector<httpHeader> headers = {}) {
    vector<httpHeader> bodyHeaders = {
            {"Content-Type",   "text/plain"},
            {"Content-Length", to_string(body.length())}
    };
    headers.insert(headers.end(), bodyHeaders.begin(), bodyHeaders.end());
    return buildRequest(method, path, headers) + body;
}

string readResponse(tcp::socket &socket, string request) {
    size_t request_length = request.length();
    boost::asio::write(socket, boost::asio::buffer(request, request_length));

    string response;
    char buff[1024];
    boost::system::error_code error;
    while (!error) {
        std::size_t bytes = read(socket, buffer(buff), error);
        response += string(buff, bytes);
    }
    return response;
}

string send(const string &method, const string &path, const string &requestBody = "") {
    try {
        auto socket = connectSocket();
        string request;
        string fullPath = rootPath + "/" + path;
        if (requestBody.empty())
            request = buildRequest(method, fullPath);
        else
            request = buildRequest(method, fullPath, requestBody);
        return readResponse(socket, request);
    } catch (exception &e) {
        return e.what();
    }
}

string sendGet(const string &path) {
    return send(GET_METHOD, path);
}

string sendPost(const string &path, const string &responseBody) {
    return send(POST_METHOD, path, responseBody);
}

string sendPut(const string &path, const string &responseBody) {
    return send(PUT_METHOD, path, responseBody);
}

string sendDelete(const string &path) {
//    auto socket = connectSocket();
//    string request = buildRequest(DELETE_METHOD, rootPath);
    return "sorry, not supported yet.";
}

